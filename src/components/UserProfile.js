import React from 'react'

const UserProfile = () => {
  return (
    <div className="min-h-screen flex items-center justify-center">
      <h1 className="text-3xl">User Profile Page</h1>
    </div>
  )
}

export default UserProfile
