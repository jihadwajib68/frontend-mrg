import React, { useState, useEffect } from 'react'
import axios from 'axios'

const OfficerDashboard = () => {
  const [requests, setRequests] = useState([])
  const [item, setItem] = useState('')
  const [quantity, setQuantity] = useState('')
  const [selectedProof, setSelectedProof] = useState(null)

  useEffect(() => {
    axios
      .get('https://test-programmer-backend.vercel.app/api/requests')
      .then((response) => setRequests(response.data))
      .catch((error) => console.error('Error fetching requests:', error))
  }, [])

  const handleSubmit = () => {
    axios
      .post('https://test-programmer-backend.vercel.app/api/requests', {
        item,
        quantity,
      })
      .then((response) => {
        setRequests([...requests, response.data])
        setItem('')
        setQuantity('')
      })
      .catch((error) => console.error('Error creating request:', error))
  }

  const handleViewProof = (proof) => {
    setSelectedProof(proof)
  }

  const handleCloseProof = () => {
    setSelectedProof(null)
  }

  return (
    <div className="min-h-screen bg-gray-100 p-8">
      <h1 className="text-2xl font-bold mb-4 text-center">Officer Dashboard</h1>
      <div className="mb-4 flex justify-center">
        <input
          type="text"
          placeholder="Item"
          value={item}
          onChange={(e) => setItem(e.target.value)}
          className="p-2 border border-gray-300 rounded mr-2"
        />
        <input
          type="number"
          placeholder="Quantity"
          value={quantity}
          onChange={(e) => setQuantity(e.target.value)}
          className="p-2 border border-gray-300 rounded mr-2"
        />
        <button
          onClick={handleSubmit}
          className="bg-blue-500 text-white py-2 px-4 rounded"
        >
          Submit
        </button>
      </div>
      <table className="min-w-full bg-white">
        <thead>
          <tr>
            <th className="py-2 px-4 border-b-2 border-gray-300">Item</th>
            <th className="py-2 px-4 border-b-2 border-gray-300">Quantity</th>
            <th className="py-2 px-4 border-b-2 border-gray-300">Status</th>
            <th className="py-2 px-4 border-b-2 border-gray-300">Proof</th>
          </tr>
        </thead>
        <tbody>
          {requests.map((request) => (
            <tr key={request._id}>
              <td className="text-center py-2 px-4 border-b border-gray-300">
                {request.item}
              </td>
              <td className="text-center py-2 px-4 border-b border-gray-300">
                {request.quantity}
              </td>
              <td className="text-center py-2 px-4 border-b border-gray-300">
                {request.status}
              </td>
              <td className="text-center py-2 px-4 border-b border-gray-300">
                {request.transferProof ? (
                  <button
                    onClick={() => handleViewProof(request.transferProof)}
                    className="text-blue-500"
                  >
                    View Proof
                  </button>
                ) : (
                  'No Proof'
                )}
              </td>
            </tr>
          ))}
        </tbody>
      </table>

      {selectedProof && (
        <div className="fixed inset-0 flex items-center justify-center bg-gray-900 bg-opacity-50">
          <div className="bg-dark p-8 rounded shadow-lg max-w-md mx-auto">
            <h2 className="text-xl font-bold mb-4 text-center">
              Proof of Payment
            </h2>
            <img
              src={`http://localhost:4000/${selectedProof}`}
              alt="Proof"
              className="mb-4 mx-auto max-w-full h-auto"
              style={{ maxWidth: '300px' }}
            />
            <button
              onClick={handleCloseProof}
              className="bg-gray-500 text-white py-2 px-4 rounded"
            >
              Close
            </button>
          </div>
        </div>
      )}
    </div>
  )
}

export default OfficerDashboard
