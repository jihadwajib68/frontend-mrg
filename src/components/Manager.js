import React, { useState, useEffect } from 'react'
import axios from 'axios'

const ManagerDashboard = () => {
  const [requests, setRequests] = useState([])
  const [rejectionReason, setRejectionReason] = useState('')
  const [selectedRequest, setSelectedRequest] = useState(null)
  const [selectedProof, setSelectedProof] = useState(null)

  useEffect(() => {
    axios
      .get('https://test-programmer-backend.vercel.app/api/requests')
      .then((response) => setRequests(response.data))
      .catch((error) => console.error('Error fetching requests:', error))
  }, [])

  const handleApprove = (id) => {
    axios
      .put(`https://test-programmer-backend.vercel.app/api/requests/${id}`, {
        approvedByManager: true,
        status: 'approved',
      })
      .then(() => setRequests(requests.filter((request) => request._id !== id)))
      .catch((error) => console.error('Error approving request:', error))
  }

  const handleReject = () => {
    if (!selectedRequest) return

    axios
      .put(
        `https://test-programmer-backend.vercel.app/api/requests/${selectedRequest._id}`,
        {
          approvedByManager: false,
          status: 'rejected',
          rejectionReason,
        }
      )
      .then(() => {
        setRequests(
          requests.filter((request) => request._id !== selectedRequest._id)
        )
        setSelectedRequest(null)
        setRejectionReason('')
      })
      .catch((error) => console.error('Error rejecting request:', error))
  }

  const handleViewProof = (proof) => {
    setSelectedProof(proof)
  }

  const handleCloseProof = () => {
    setSelectedProof(null)
  }

  return (
    <div className="min-h-screen bg-gray-100 p-8">
      <h1 className="text-2xl font-bold mb-4 text-center">Manager Dashboard</h1>
      <table className="min-w-full bg-white">
        <thead>
          <tr>
            <th className="py-2 px-4 border-b-2 border-gray-300">Item</th>
            <th className="py-2 px-4 border-b-2 border-gray-300">Quantity</th>
            <th className="py-2 px-4 border-b-2 border-gray-300">Status</th>
            <th className="py-2 px-4 border-b-2 border-gray-300">Proof</th>
            <th className="py-2 px-4 border-b-2 border-gray-300">Actions</th>
          </tr>
        </thead>
        <tbody>
          {requests.map((request) => (
            <tr key={request._id}>
              <td className="text-center py-2 px-4 border-b border-gray-300">
                {request.item}
              </td>
              <td className="text-center py-2 px-4 border-b border-gray-300">
                {request.quantity}
              </td>
              <td className="text-center py-2 px-4 border-b border-gray-300">
                {request.status}
              </td>
              <td className="text-center py-2 px-4 border-b border-gray-300">
                {request.transferProof ? (
                  <button
                    onClick={() => handleViewProof(request.transferProof)}
                    className="text-blue-500"
                  >
                    View Proof
                  </button>
                ) : (
                  'No Proof'
                )}
              </td>
              <td className="text-center py-2 px-4 border-b border-gray-300">
                {request.status === 'pending' && (
                  <>
                    <button
                      onClick={() => handleApprove(request._id)}
                      className="bg-green-500 text-white py-1 px-2 rounded ml-2"
                    >
                      Approve
                    </button>
                    <button
                      onClick={() => setSelectedRequest(request)}
                      className="bg-red-500 text-white py-1 px-2 rounded ml-2"
                    >
                      Reject
                    </button>
                  </>
                )}
              </td>
            </tr>
          ))}
        </tbody>
      </table>

      {selectedRequest && (
        <div className="fixed inset-0 flex items-center justify-center bg-gray-900 bg-opacity-50">
          <div className="bg-white p-8 rounded shadow-lg">
            <h2 className="text-xl font-bold mb-4">Reject Request</h2>
            <textarea
              placeholder="Enter rejection reason"
              value={rejectionReason}
              onChange={(e) => setRejectionReason(e.target.value)}
              className="w-full p-2 border border-gray-300 rounded mb-4"
            />
            <button
              onClick={handleReject}
              className="bg-red-500 text-white py-2 px-4 rounded mr-2"
            >
              Submit
            </button>
            <button
              onClick={() => setSelectedRequest(null)}
              className="bg-gray-500 text-white py-2 px-4 rounded"
            >
              Cancel
            </button>
          </div>
        </div>
      )}

      {selectedProof && (
        <div className="fixed inset-0 flex items-center justify-center bg-gray-900 bg-opacity-50">
          <div className="bg-dark p-8 rounded shadow-lg max-w-md mx-auto">
            <h2 className="text-xl font-bold mb-4 text-center">
              Proof of Payment
            </h2>
            <img
              src={`http://localhost:4000/${selectedProof}`}
              alt="Proof"
              className="mb-4 mx-auto max-w-full h-auto"
              style={{ maxWidth: '300px' }}
            />
            <button
              onClick={handleCloseProof}
              className="bg-gray-500 text-white py-2 px-4 rounded"
            >
              Close
            </button>
          </div>
        </div>
      )}
    </div>
  )
}

export default ManagerDashboard
