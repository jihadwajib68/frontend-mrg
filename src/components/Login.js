import React, { useState, useContext } from 'react'
import axios from 'axios'
import { useNavigate, Link } from 'react-router-dom'
import { AuthContext } from '../context/AuthContext'

const Login = () => {
  const [username, setUsername] = useState('')
  const [password, setPassword] = useState('')
  const navigate = useNavigate()
  const { setRole } = useContext(AuthContext)

  const handleSubmit = () => {
    axios
      .post('https://test-programmer-backend.vercel.app/api/users/login', {
        username,
        password,
      })
      .then((response) => {
        const role = response.data.role
        localStorage.setItem('role', role)
        localStorage.setItem('username', username)
        localStorage.setItem('password', password)
        setRole(role)
        if (role === 'Admin') navigate('/admin')
        else if (role === 'Officer') navigate('/officer')
        else if (role === 'Manager') navigate('/manager')
        else if (role === 'Finance') navigate('/finance')
      })
      .catch((error) => console.error(error))
  }

  const handleKeyPress = (e) => {
    if (e.key === 'Enter') {
      handleSubmit()
    }
  }

  return (
    <div className="min-h-screen flex items-center justify-center bg-gray-100">
      <div className="bg-white p-8 rounded-lg shadow-lg w-96">
        <h1 className="text-2xl font-bold mb-4">Login</h1>
        <input
          type="text"
          placeholder="Username"
          value={username}
          onChange={(e) => setUsername(e.target.value)}
          className="mb-2 p-2 border border-gray-300 rounded w-full"
        />
        <input
          type="password"
          placeholder="Password"
          value={password}
          onKeyPress={handleKeyPress}
          onChange={(e) => setPassword(e.target.value)}
          className="mb-2 p-2 border border-gray-300 rounded w-full"
        />
        <button
          onClick={handleSubmit}
          className="bg-blue-500 text-white py-2 px-4 rounded w-full"
        >
          Login
        </button>
        <div className="mt-4 text-center">
          <p>
            Don't have an account?{' '}
            <Link to="/signup" className="text-blue-500">
              Sign up
            </Link>
          </p>
        </div>
      </div>
    </div>
  )
}

export default Login
