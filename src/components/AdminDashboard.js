import React, { useState, useEffect } from 'react'
import axios from 'axios'

const AdminDashboard = () => {
  const [users, setUsers] = useState([])
  const [requests, setRequests] = useState([])
  const [selectedProof, setSelectedProof] = useState(null)
  const [editingUser, setEditingUser] = useState(null)
  const [editingRequest, setEditingRequest] = useState(null)

  useEffect(() => {
    axios
      .get('https://test-programmer-backend.vercel.app/api/users')
      .then((response) => setUsers(response.data))
      .catch((error) => console.error('Error fetching users:', error))

    axios
      .get('https://test-programmer-backend.vercel.app/api/requests')
      .then((response) => setRequests(response.data))
      .catch((error) => console.error('Error fetching requests:', error))
  }, [])

  const handleDeleteUser = (id) => {
    axios
      .delete(`https://test-programmer-backend.vercel.app/api/users/${id}`)
      .then(() => setUsers(users.filter((user) => user._id !== id)))
      .catch((error) => console.error('Error deleting user:', error))
  }

  const handleDeleteRequest = (id) => {
    axios
      .delete(`https://test-programmer-backend.vercel.app/api/requests/${id}`)
      .then(() => setRequests(requests.filter((request) => request._id !== id)))
      .catch((error) => console.error('Error deleting request:', error))
  }

  const handleViewProof = (proof) => {
    setSelectedProof(proof)
  }

  const handleCloseProof = () => {
    setSelectedProof(null)
  }

  const handleUpdateUser = (id) => {
    const username = prompt('Enter new username:', editingUser.username)
    const role = prompt('Enter new role:', editingUser.role)
    axios
      .put(`https://test-programmer-backend.vercel.app/api/users/${id}`, {
        username,
        role,
      })
      .then((response) => {
        setUsers(
          users.map((user) =>
            user._id === id ? { ...user, username, role } : user
          )
        )
        setEditingUser(null)
      })
      .catch((error) => console.error('Error updating user:', error))
  }

  const handleUpdateRequest = (id) => {
    const item = prompt('Enter new item:', editingRequest.item)
    const quantity = prompt('Enter new quantity:', editingRequest.quantity)
    const status = prompt('Enter new status:', editingRequest.status)
    const rejectionReason = prompt(
      'Enter new rejection reason:',
      editingRequest.rejectionReason
    )
    axios
      .put(`https://test-programmer-backend.vercel.app/api/requests/${id}`, {
        item,
        quantity,
        status,
        rejectionReason,
      })
      .then((response) => {
        setRequests(
          requests.map((request) =>
            request._id === id
              ? { ...request, item, quantity, status, rejectionReason }
              : request
          )
        )
        setEditingRequest(null)
      })
      .catch((error) => console.error('Error updating request:', error))
  }

  return (
    <div className="min-h-screen bg-gray-100 p-8">
      <h1 className="text-2xl font-bold mb-4 text-center">Admin Dashboard</h1>
      <h2 className="text-xl font-bold mb-4">Users</h2>
      <table className="min-w-full bg-white mb-8">
        <thead>
          <tr>
            <th className="py-2 px-4 border-b-2 border-gray-300">Username</th>
            <th className="py-2 px-4 border-b-2 border-gray-300">Role</th>
            <th className="py-2 px-4 border-b-2 border-gray-300">Actions</th>
          </tr>
        </thead>
        <tbody>
          {users.map((user) => (
            <tr key={user._id}>
              <td className="text-center py-2 px-4 border-b border-gray-300">
                {user.username}
              </td>
              <td className="text-center py-2 px-4 border-b border-gray-300">
                {user.role}
              </td>
              <td className="text-center py-2 px-4 border-b border-gray-300">
                <button
                  onClick={() => setEditingUser(user)}
                  className="bg-yellow-500 text-white py-1 px-2 rounded mr-2"
                >
                  Edit
                </button>
                <button
                  onClick={() => handleDeleteUser(user._id)}
                  className="bg-red-500 text-white py-1 px-2 rounded"
                >
                  Delete
                </button>
              </td>
            </tr>
          ))}
        </tbody>
      </table>

      <h2 className="text-xl font-bold mb-4">Requests</h2>
      <table className="min-w-full bg-white">
        <thead>
          <tr>
            <th className="py-2 px-4 border-b-2 border-gray-300">Item</th>
            <th className="py-2 px-4 border-b-2 border-gray-300">Quantity</th>
            <th className="py-2 px-4 border-b-2 border-gray-300">Status</th>
            <th className="py-2 px-4 border-b-2 border-gray-300">
              Rejection Reason
            </th>
            <th className="py-2 px-4 border-b-2 border-gray-300">Proof</th>
            <th className="py-2 px-4 border-b-2 border-gray-300">Actions</th>
          </tr>
        </thead>
        <tbody>
          {requests.map((request) => (
            <tr key={request._id}>
              <td className="text-center py-2 px-4 border-b border-gray-300">
                {request.item}
              </td>
              <td className="text-center py-2 px-4 border-b border-gray-300">
                {request.quantity}
              </td>
              <td className="text-center py-2 px-4 border-b border-gray-300">
                {request.status}
              </td>
              <td className="text-center py-2 px-4 border-b border-gray-300">
                {request.rejectionReason}
              </td>
              <td className="text-center py-2 px-4 border-b border-gray-300">
                {request.transferProof ? (
                  <button
                    onClick={() => handleViewProof(request.transferProof)}
                    className="text-blue-500"
                  >
                    View Proof
                  </button>
                ) : (
                  'No Proof'
                )}
              </td>
              <td className="text-center py-2 px-4 border-b border-gray-300">
                <button
                  onClick={() => setEditingRequest(request)}
                  className="bg-yellow-500 text-white py-1 px-2 rounded mr-2"
                >
                  Edit
                </button>
                <button
                  onClick={() => handleDeleteRequest(request._id)}
                  className="bg-red-500 text-white py-1 px-2 rounded ml-2"
                >
                  Delete
                </button>
              </td>
            </tr>
          ))}
        </tbody>
      </table>

      {editingUser && (
        <div className="fixed inset-0 flex items-center justify-center bg-gray-900 bg-opacity-50">
          <div className="bg-white p-8 rounded shadow-lg">
            <h2 className="text-xl font-bold mb-4">Edit User</h2>
            <input
              type="text"
              placeholder="Username"
              defaultValue={editingUser.username}
              onChange={(e) =>
                setEditingUser({ ...editingUser, username: e.target.value })
              }
              className="w-full p-2 border border-gray-300 rounded mb-4"
            />
            <input
              type="text"
              placeholder="Role"
              defaultValue={editingUser.role}
              onChange={(e) =>
                setEditingUser({ ...editingUser, role: e.target.value })
              }
              className="w-full p-2 border border-gray-300 rounded mb-4"
            />
            <button
              onClick={() => handleUpdateUser(editingUser._id)}
              className="bg-blue-500 text-white py-2 px-4 rounded mr-2"
            >
              Save
            </button>
            <button
              onClick={() => setEditingUser(null)}
              className="bg-gray-500 text-white py-2 px-4 rounded"
            >
              Cancel
            </button>
          </div>
        </div>
      )}

      {editingRequest && (
        <div className="fixed inset-0 flex items-center justify-center bg-gray-900 bg-opacity-50">
          <div className="bg-white p-8 rounded shadow-lg">
            <h2 className="text-xl font-bold mb-4">Edit Request</h2>
            <input
              type="text"
              placeholder="Item"
              defaultValue={editingRequest.item}
              onChange={(e) =>
                setEditingRequest({
                  ...editingRequest,
                  item: e.target.value,
                })
              }
              className="w-full p-2 border border-gray-300 rounded mb-4"
            />
            <input
              type="number"
              placeholder="Quantity"
              defaultValue={editingRequest.quantity}
              onChange={(e) =>
                setEditingRequest({
                  ...editingRequest,
                  quantity: e.target.value,
                })
              }
              className="w-full p-2 border border-gray-300 rounded mb-4"
            />
            <input
              type="text"
              placeholder="Status"
              defaultValue={editingRequest.status}
              onChange={(e) =>
                setEditingRequest({
                  ...editingRequest,
                  status: e.target.value,
                })
              }
              className="w-full p-2 border border-gray-300 rounded mb-4"
            />
            <input
              type="text"
              placeholder="Rejection Reason"
              defaultValue={editingRequest.rejectionReason}
              onChange={(e) =>
                setEditingRequest({
                  ...editingRequest,
                  rejectionReason: e.target.value,
                })
              }
              className="w-full p-2 border border-gray-300 rounded mb-4"
            />
            <button
              onClick={() => handleUpdateRequest(editingRequest._id)}
              className="bg-blue-500 text-white py-2 px-4 rounded mr-2"
            >
              Save
            </button>
            <button
              onClick={() => setEditingRequest(null)}
              className="bg-gray-500 text-white py-2 px-4 rounded"
            >
              Cancel
            </button>
          </div>
        </div>
      )}

      {selectedProof && (
        <div className="fixed inset-0 flex items-center justify-center bg-gray-900 bg-opacity-50">
          <div className="bg-dark p-8 rounded shadow-lg max-w-md mx-auto">
            <h2 className="text-xl font-bold mb-4 text-center">
              Proof of Payment
            </h2>
            <img
              src={`http://localhost:4000/${selectedProof}`}
              alt="Proof"
              className="max-w-full h-auto mx-auto mb-4"
              style={{ maxWidth: '300px' }}
            />
            <button
              onClick={handleCloseProof}
              className="bg-gray-500 text-white py-2 px-4 rounded"
            >
              Close
            </button>
          </div>
        </div>
      )}
    </div>
  )
}

export default AdminDashboard
